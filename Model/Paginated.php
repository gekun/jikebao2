<?php

namespace AzureSpring\Jikebao\Model;

interface Paginated
{
    public function getData(): array;
    public function getTotal(): int;
}
