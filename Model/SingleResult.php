<?php

namespace AzureSpring\Jikebao\Model;

use AzureSpring\Jikebao\Annotation\Template;

/** @Template({"T"}) */
class SingleResult extends Result
{
    private $data;

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }
}
