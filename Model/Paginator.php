<?php

namespace AzureSpring\Jikebao\Model;

class Paginator
{
    /**
     * @var int|null
     */
    private $page;

    /**
     * @var int|null
     */
    private $size;

    /**
     * @return int|null
     */
    public function getPage(): ?int
    {
        return $this->page;
    }

    /**
     * @param int|null $page
     *
     * @return $this
     */
    public function setPage(?int $page): self
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getSize(): ?int
    {
        return $this->size;
    }

    /**
     * @param int|null $size
     *
     * @return $this
     */
    public function setSize(?int $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function toParams(): array
    {
        return [
            'page' => $this->getPage(),
            'size' => $this->getSize(),
        ];
    }
}
