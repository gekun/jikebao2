<?php

namespace AzureSpring\Jikebao\Model;

class Order
{
    /** @var int */
    private $id;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
