<?php

namespace AzureSpring\Jikebao\Model;

use phpDocumentor\Reflection\Type;

class OrderOptions
{
    /** @var string|null */
    private $permanentId;

    /** @var string */
    private $name = '';

    /** @var string */
    private $mobile = '';

    /** @var string|null */
    private $idNumber;

    /** @var string|null */
    private $sellPrice;

    /** @var string|null */
    private $costPrice;


    /** @var \DateTimeInterface|null */
    private $date;

    public static function create()
    {
        return new OrderOptions();
    }

    /**
     * @return string|null
     */
    public function getPermanentId(): ?string
    {
        return $this->permanentId;
    }

    /**
     * @param string|null $permanentId
     *
     * @return $this
     */
    public function setPermanentId(?string $permanentId): OrderOptions
    {
        $this->permanentId = $permanentId;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): OrderOptions
    {
        $this->name = preg_replace('/\p{S}/u', '', $name);

        return $this;
    }

    /**
     * @return string
     */
    public function getMobile(): string
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     *
     * @return $this
     */
    public function setMobile(string $mobile): OrderOptions
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getIdNumber(): ?string
    {
        return $this->idNumber;
    }

    /**
     * @param string|null $idNumber
     *
     * @return $this
     */
    public function setIdNumber(?string $idNumber): self
    {
        $this->idNumber = $idNumber;

        return $this;
    }

    /**
     * @param string|null $sellPrice
     *
     * @return $this
     */
    public function setSellPrice(?string $sellPrice): self
    {
        $this->sellPrice = $sellPrice;

        return $this;
    }

    public function getSellPrice(): ?string
    {
        return $this->sellPrice;
    }

    /**
     * @param string|null $costPrice
     *
     * @return $this
     */
    public function setCostPrice(?string $costPrice): self
    {
        $this->costPrice = $costPrice;

        return $this;
    }

    public function getCostPrice(): ?string
    {
        return $this->costPrice;
    }



    /**
     * @return \DateTimeInterface|null
     */
    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    /**
     * @param \DateTimeInterface|null $date
     *
     * @return $this
     */
    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
    
    public function toParams(): array
    {
        $visitPerson = [];
        if($this->getIdNumber()) {
            $visitPerson['credentials'] = $this->getIdNumber();
            $visitPerson['credentialsType'] = 'Idcard';
            $visitPerson['visitName'] = $this->getName();
            $visitPerson['visitMobile'] = $this->getMobile();
        }
        return array_filter([
            'outOrderId' => $this->getPermanentId(),
            'contactName' => $this->getName(),
            'contactMobile' => $this->getMobile(),
            'useDate' => $this->getDate() ? $this->getDate()->format('Y-m-d') : null,
            'sellPrice' => $this->getSellPrice(),
            'distPrice' => $this->getCostPrice(),
            'visitPerson' => [$visitPerson],
        ]);
    }
}
