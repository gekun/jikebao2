<?php

namespace AzureSpring\Jikebao\Model;

use AzureSpring\Jikebao\Annotation\Template;

/** @Template({"T"}) */
class PaginatedResult extends Result implements Paginated
{
    /**
     * @var array
     */
    private $data;

    /**
     * @var int
     */
    private $total;

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }
}
