<?php

namespace AzureSpring\Jikebao\Notification;

abstract class OrderNotification extends AbstractNotification
{
    /** @var string */
    protected $orderId;

    public function __construct(string $orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return string
     */
    public function getOrderId(): string
    {
        return $this->orderId;
    }
}
